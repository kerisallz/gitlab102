let x = 2;
console.log(`First value of x: ${x}`);
x = x / 3;
console.log(`Second value of x: ${x}`);

let y = 8;

y += 4;
console.log(`First value of y: ${y}`);
y -= 17;
console.log(`Second value of y: ${y}`);
y *= 55;
console.log(`Third value of y: ${y}`);
y /= 8;
console.log(`Fourth value of y: ${y}`);
``