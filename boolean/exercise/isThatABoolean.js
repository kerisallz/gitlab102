const a = 'blag blagh';
const b = 100 + 23 ** 2;
const c = false && true || (false && false);
let d; 
const e = 3 - 17 < 44;

console.log(`a: ${typeof(a) === 'boolean'}`);
console.log(`b: ${typeof(b) === 'boolean'}`);
console.log(`c: ${typeof(c) === 'boolean'}`);
console.log(`d: ${typeof(d) === 'boolean'}`);
console.log(`e: ${typeof(e) === 'boolean'}`);

