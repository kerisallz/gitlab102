const ex1 = (false || true) && false; 
console.log(ex1); // Should be False

const ex2 = true || (false && true || false);
console.log(ex2); // SHould be TRUE

const ex3 = false || (true && !(false || true)) && false
console.log(ex3); // Should be False
