const textTest1 = 'dsadfdsacmniwietewcdsa';
const textTest2 = '';

const test1 = textTest1.length > 0;
const test2 = textTest2.length > 0;

console.log(`\n${test1}`); // Result in TEXT ;
console.log(test2); // Result in value = False;

const msg1 = 'asdokqosa';
const msg2 = '';
const msg3 = '?';

console.log(`\nString 1 os empty? : ${msg1 === ''}`);
console.log(`\nString 2 os empty? : ${msg2 === ''}`);
console.log(`\nString 3 os empty? : ${msg3 === ''}`);
