// Operator always do && fist.

const ex1 = false || true;
console.log(ex1); // Should be TRUE

const ex2 = true && true || false;
console.log(ex2); // Should be TRUE

const ex3 = false || false || false && true;
console.log(ex3); // Should be False

const ex4 = true && false || true || false && true;
console.log(ex4); // Should be TRUE
