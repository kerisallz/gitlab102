const DragonHP = 100;

const shot1DMG = 25;
const shot2DMG = 50;
const shot3DMG = 27;
const TotalDMG = (shot1DMG + shot2DMG + shot3DMG);

console.log(`The dragon was beaten: ${DragonHP - TotalDMG <= 0} `);
console.log(`The left over HP : ${DragonHP - TotalDMG} hp`);
