const heightINMetre = 150;
const weightINkg = 70;

const isHeightBig = heightINMetre > 160; // True
const isWeightBig = weightINkg > 80; // True 

console.log(
    `Big Test Height: ${heightINMetre} metre, Weight : ${weightINkg} kg`
);

const IAmBig = isWeightBig || isHeightBig; 

// operator = "||" = หรือ

/*
T T = T
T F = T
F T = T
F F = F`
*/

console.log(`Am I big: ${IAmBig}`);

// If we are small

const isHeightSmall = heightINMetre < 160; //False
const isWeightSmall = weightINkg < 80; //False

console.log(`------------`);
console.log(`SMALL TEST: Height: ${heightINMetre} metre, Weight: ${weightINkg} kg.`);

const IamSmall = isHeightSmall && isHeightSmall;

console.log(`Am I small: ${IamSmall}`)



